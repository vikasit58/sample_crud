<?php
    /* Description of index.php: index.php list all employee's data
    *  project version: 1.1.0
    *  @author Vikas Singh <singh.vikas93@gmail.com>
    *  @client company: ESS
    *  @date created: Feb 01, 2019 22:25:21 PM
    *  @date last modified: Feb 01, 2019 23:22:20 PM
    *  ******************************************************************************
    */

	//start session
	session_start();
	//Model.php having custom crud function for db operations including the database connection instance
	include_once('Model.php');
    // instance of model class
    $model = new Model();
    
	//fetch records from database
	$query = "SELECT * FROM employee";
	$result = $model->getRecords($query);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Test Crud Operation Created By Vikas Singh</title>
</head>
<body>
<div class="container">
	<h1 class="page-header text-center">Test Crud Operation Created By Vikas Singh</h1>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<?php
				if(isset($_SESSION['message'])){
					?>
						<div class="alert alert-info text-center">
							<?php echo $_SESSION['message']; ?>
						</div>
					<?php
					// destroying the session values
					unset($_SESSION['message']);
				}
			
			?>
            // add new employee form redirect
			<a href="add.php" class="btn btn-primary">Add New Employee</a><br>
            // table for employee listing
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Firstname</th>
						<th>Lastname</th>
						<th>Mobile</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
                       // get all employee list
						foreach ($result as $key => $row) {
							?>
							<tr>
								<td><?php echo $row['id']; ?></td>
								<td><?php echo $row['firstname']; ?></td>
								<td><?php echo $row['lastname']; ?></td>
								<td><?php echo $row['mobile']; ?></td>
								<td><a href="edit.php/<?php echo $row['id']; ?>" class="btn btn-success">Edit</a> | 
									<a href="delete.php/<?php echo $row['id']; ?>" class="btn btn-danger">Delete</a>
								</td>
							</tr>
							<?php     
					    }
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>