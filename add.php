<?php
 /*  Description of add.php: Add new member to the database
  *  project version: 1.1.0
  *  @author Vikas Singh <singh.vikas93@gmail.com>
  *  @client company: ESS
  *  @date created: Feb 01, 2019 23:00:01 PM
  *  @date last modified: Feb 01, 2019 23:15:31 PM
  *  ******************************************************************************
 */


//start session
session_start();

//Model.php having custom crud function for db operations including the database connection instance
include_once('Model.php');

// instance of model class
$model = new Model();
 
if(isset($_POST['add'])) { 
    //r_escape_string used to Escapes special characters for security   
    $firstName = $model->r_escape_string($_POST['firstname']);
    $lastName = $model->r_escape_string($_POST['lastname']);
    $mobileNo = $model->r_escape_string($_POST['mobileno']);
        
    //insert data to database
    $sql = "INSERT INTO employee (firstname, lastname, mobile) VALUES ('$firstname','$lastName','$mobileNo')";
    
    // calling execute function using model class object for data insertion
    if($model->insert($sql)){
        $_SESSION['message'] = 'Employee added successfully';
    }
    else{
        $_SESSION['message'] = 'Employee not added';
    }
    // redirect to employee list page    
    header('location: index.php');
}
else{
    // required fields alert
    $_SESSION['message'] = 'Please fill required fields first';
    // redirect to employee list page
    header('location: index.php');
}
?>


<!--  HTML Form  to add new employee -->

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
 
 <table class='table table-hover table-responsive table-bordered'>

     <tr>
         <td>First Name</td>
         <td><input type='text' name='firstname' class='form-control' required='required' /></td>
     </tr>

     <tr>
         <td>Last name</td>
         <td><input type='text' name='lastname' class='form-control' required='required' /></td>
     </tr>

     <tr>
         <td>Mobile Number</td>
         <td><input type='text' name='mobileno' class='form-control' required='required' /></td>
     </tr>

     <tr>
         <td></td>
         <td>
             <button type="submit" class="btn btn-primary">Add Employee</button>
         </td>
     </tr>

 </table>
</form>
