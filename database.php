<?php
 /*  Description of Database.php: Database configuration file used to create instance database class
  *  project version: 1.1.0
  *  @author Vikas Singh <singh.vikas93@gmail.com>
  *  @client company: ESS
  *  @date created: Feb 01, 2019 22:09:01 PM
  *  @date last modified: Feb 01, 2019 22:59:31 PM
  *  ******************************************************************************
 */

 // database connection class
class Database 
{   
    // database credentials
    private $host = 'localhost';
    private $username = 'root';
    private $password = '';
    private $dbname = 'employee'; // database name
    
    protected $connection;
    
    //constructor function 
    public function __construct(){

        if (!isset($this->connection)) {
            
            $this->connection = new mysqli($this->host, $this->username, $this->password, $this->dbname);
            
            if (!$this->connection) {
                echo 'Cannot connect to database server';
                exit;
            }            
        }    
        // return database instance
        return $this->connection;
    }
}
?>