<?php
 /*  Description of Model.php: Model.php having custom crud function for db operations including the database connection instance
  *  project version: 1.1.0
  *  @author Vikas Singh <singh.vikas93@gmail.com>
  *  @client company: ESS
  *  @date created: Feb 01, 2019 22:20:21 PM
  *  @date last modified: Feb 01, 2019 23:10:31 PM
  *  ******************************************************************************
 */
<?php
//Database configuration file used to create instance database class
include_once('Database.php');

// inheriting Database class 
class Model extends Database
{   
    // constructor function to execute with instance Model class
    public function __construct(){

        parent::__construct();
    }

    // fetch records from database
    public function getRecords($sql){

        $query = $this->connection->query($sql);
        
        if ($query == false) {
            return false;
        } 
        
        $rows = array();
        
        while ($row = $query->fetch_array()) {
            $rows[] = $row;
        }
        
        return $rows;
    }

    // insert record in database    
    public function insert($sql){

        $query = $this->connection->query($sql);
        
        if ($query == false) {
            return false;
        } else {
            return true;
        }        
    }

    // Escapes special characters for security
    public function r_escape_string($value){
        
        return $this->connection->real_escape_string($value);
    }
}
?>